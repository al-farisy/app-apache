---
Etape suivante après l'installation d'apache la configuration du fichier httpd.conf

#On s'assure que apache soit installé avec la dernière version
- name: 1 --- > Derniere version d'apache
  yum:
    name: httpd
    state: latest
    update_cache: yes

#Attribution du droit d'écriture

- name: Give writable mode to http folder
  file:
    path: /var/www/html
    state: directory
    mode: '0755'

#Configuration du fichier smile.com.conf :
# 1 -- > Création de vhost
# 2 -- > le DocumentRoot
# 3 -- > serveralias
# 4 -- > Customlog

- name: apache2 virtualhost
  lineinfile:
    dest: /etc/httpd/var/www/html/smile.com.conf
    line: '{{ item }}'
  with_items:
    - '<VirtualHost *:80>'
    - 'ServerName www.smile.com'
    - 'ServerAlias smile.com'
    - 'DocumentRoot /var/www/smile.com/public_html'
    - 'ErrorLog /var/www/smile.com/error.log'
    - 'CustomLog /var/www/smile.com/requests.log combined'
    - '</VirtualHost>'

#Redemarrer le service apache
- name: 3/ ensure apache is running
  service:
    name: httpd
    state: started

